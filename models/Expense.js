const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ExpenseSchema = new Schema({
  title: String,
  category: String,
  date: String,
  value: Number
})

module.exports = mongoose.model('Expense', ExpenseSchema);