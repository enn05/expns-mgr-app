const express = require('express');

const ExpenseRoute = express.Router();

const ExpenseModel = require('../models/Expense');


//add new expense
ExpenseRoute.post('/addexpense', async (req, res) => {
  try {
    let newExpense = ExpenseModel({
      title: req.body.title,
      category: req.body.category,
      date: req.body.date,
      value: req.body.value
    })

    newExpense = await newExpense.save();
    res.send(newExpense);
  } catch (e) {
    console.log(e);
  }
});

//get all expenses
ExpenseRoute.get('/showexpense', async (req, res) => {
  try {
    let expenses = await ExpenseModel.find();
    res.send(expenses);
  } catch (e) {
    console.log(e);
  }
});

//update expense
ExpenseRoute.put('/updateexpense/:id', async (req, res) => {
  try {
    let expense = await ExpenseModel.findById(req.params.id);
    if (!expense) {
      return res.status(404).send(`Cannot find expense`);
    }

    let condition = { _id: req.params.id }
    let updates = {
      title: req.body.title,
      category: req.body.category,
      date: req.body.date,
      value: req.body.value
    }

    let updatedExpense = await ExpenseModel.findByIdAndUpdate(condition, updates, { new: true })
    res.send(updatedExpense)
  } catch (e) {
    console.log(e)
  }
})

//delete expense
ExpenseRoute.delete('/deleteexpense/:id', async(req, res)=>{
  try {
    let deletedExpense = await ExpenseModel.findByIdAndDelete(req.params.id);
    res.send(deletedExpense);
  } catch (e) {
    console.log(e)
  }
})

module.exports = ExpenseRoute;