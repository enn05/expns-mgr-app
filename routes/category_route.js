const express = require('express');

const CategoryRoute = express.Router();

const CategoryModel = require('../models/Category');


//add new category
CategoryRoute.post('/addcategory', async (req, res) => {
  try {
    let newCategory = CategoryModel({
      title: req.body.title,
      description: req.body.description,
    })

    newCategory = await newCategory.save();
    res.send(newCategory);
  } catch (e) {
    console.log(e);
  }
})

//get all categories
CategoryRoute.get('/showcategories', async (req, res) => {
  try {
    let categories = await CategoryModel.find();
    res.send(categories);
  } catch (e) {
    console.log(e)
  }
})

//update category
CategoryRoute.put('/updatecategory/:id', async (req, res) => {
  try {
    let category = await CategoryModel.findById(req.params.id);
    if (!category) {
      return res.status(404).send(`Category can't be found`);
    }

    let condition = { _id: req.params.id };
    let updates = {
      title: req.body.title,
      description: req.body.description
    }

    let updatedCategory = await CategoryModel.findByIdAndUpdate(condition, updates, { new: true });
    res.send(updatedCategory);
  } catch (e) {
    console.log(e)
  }
})

//delete category
CategoryRoute.delete('/deletecategory/:id', async (req, res) => {
  try {
    let deletedCategory = await CategoryModel.findByIdAndDelete(req.params.id);
    res.send(deletedCategory)
  } catch (e) {
    console.log(e)
  }
})

module.exports = CategoryRoute;