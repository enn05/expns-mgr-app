const express = require('express');
const app = express();
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

//connect to database
mongoose
  .connect(
    config.databaseUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    }
  )
  .then(()=>{
    console.log('Remote Database Connection Established');
  });

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

//setup server
app.listen(config.port, () => {
  console.log(`Server started on port ${config.port}`);
});


//category api
const categories = require('./routes/category_route');
app.use('/', categories);

//expense api
const expenses = require('./routes/expense_route');
app.use('/', expenses);